/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#include "output.h"
#define BLINK_GPIO 2

void app_main(void)
{
    output_create(2);
    while(1) {
        output_toggle(2);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
